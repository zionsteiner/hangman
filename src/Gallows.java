import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * Gallows pane for hangman GUI and control
 */
public class Gallows extends Pane {
    private Arc base;
    private Line line;
    private Line line2;
    private Line line3;
    private Circle head;
    private Line body;
    private Circle pin;
    private Line armLeft;
    private Line armRight;
    private Line legLeft;
    private Line legRight;

    /**
     * Builds and initializes gallows and hangman
     */
    Gallows() {
        // Gallows
        base = new Arc(100, 500, 75, 50, 0, 180);
        base.setType(ArcType.OPEN);
        base.setFill(null);
        base.setStroke(Color.BLACK);

        line = new Line(base.getCenterX(),
                base.getCenterY() - base.getRadiusY(),
                base.getCenterX(),
                base.getCenterY() - base.getRadiusY() - 250);

        line2 = new Line(line.getEndX(), line.getEndY(), line.getEndX() + 100, line.getEndY());

        line3 = new Line(line2.getEndX(), line2.getEndY(), line2.getEndX(), line2.getEndY() + 50);

        //Hangman
        head = new Circle(line3.getEndX(), line3.getEndY() + 25, 25, null);
        head.setStroke(Color.BLACK);
        head.setVisible(false);

        body = new Line(head.getCenterX(),
                head.getCenterY() + head.getRadius(),
                head.getCenterX(),
                head.getCenterY() + head.getRadius() + 75);
        body.setVisible(false);
        pin = new Circle(body.getStartX(), body.getStartY() + 20, 20, new ImagePattern(new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Brigham_Young_University_medallion.svg/300px-Brigham_Young_University_medallion.svg.png")));
        pin.setVisible(false);

        armLeft = new Line(head.getCenterX() - head.getRadius() * Math.cos(Math.toRadians(45)),
                head.getCenterY() + head.getRadius() * Math.sin(Math.toRadians(45)),
                head.getCenterX() - head.getRadius() * Math.cos(Math.toRadians(45)) - 20,
                head.getCenterY() + head.getRadius() * Math.sin(Math.toRadians(45)) + 20);
        armLeft.setVisible(false);

        armRight = new Line(head.getCenterX() - head.getRadius() * Math.cos(Math.toRadians(135)),
                head.getCenterY() + head.getRadius() * Math.sin(Math.toRadians(135)),
                head.getCenterX() - head.getRadius() * Math.cos(Math.toRadians(135)) + 20,
                head.getCenterY() + head.getRadius() * Math.sin(Math.toRadians(135)) + 20);
        armRight.setVisible(false);

        legLeft = new Line(body.getEndX(), body.getEndY(), body.getEndX() - 20, body.getEndY() + 40);
        legLeft.setVisible(false);

        legRight = new Line(body.getEndX(), body.getEndY(), body.getEndX() + 20, body.getEndY() + 40);
        legRight.setVisible(false);

        getChildren().addAll(base, line, line2, line3, head, body, pin, armLeft, armRight, legLeft, legRight);
        setBorder(new Border(new BorderStroke(Color.rgb(6, 50, 67),
                BorderStrokeStyle.SOLID,
                new CornerRadii(10),
                new BorderWidths(3))));
    }

    /**
     * Draws next part of hangman on mistake
     * @param numMistakes mistake number determines which body part is drawn
     */
    public void drawNextPart(int numMistakes) {
        switch(numMistakes) {
            case 1: drawHead();
                break;
            case 2: drawBody();
                break;
            case 3: drawArmLeft();
                break;
            case 4: drawArmRight();
                break;
            case 5: drawLegLeft();
                break;
            case 6: drawLegRight();
                break;
        }
    }

    /**
     * Draws hangman head
     */
    private void drawHead() {
        head.setVisible(true);
    }

    /**
     * Draws hangman body and pin of rival university
     */
    private void drawBody() {
        body.setVisible(true);
        pin.setVisible(true);
    }

    /**
     * Draws left arm of hangman
     */
    private void drawArmLeft() {
        armLeft.setVisible(true);
    }

    /**
     * Draws right arm of hangman
     */
    private void drawArmRight() {
        armRight.setVisible(true);
    }

    /**
     * Draws left leg of hangman
     */
    private void drawLegLeft() {
        legLeft.setVisible(true);
    }

    /**
     * Draws right leg of hangman
     */
    private void drawLegRight() {
        legRight.setVisible(true);
    }
}