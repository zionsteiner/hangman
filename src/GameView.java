import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * GameView hangman GUI pane with game-state/logic control
 * @author ZionSteiner
 */
class GameView extends BorderPane {
    private final int MAX_MISTAKES = 6;         // Max number of mistakes before game over
    private int numMistakes;                    // Current number of mistakes
    private String phrase;                      // Random hangman phrase to guess
    private ArrayList<String> guessedLetters;   // List of all letters guessed
    private ArrayList<String> wrongGuesses;     // List of incorrect guesses

    private TextField tfInput;                  // Textfield for user guess input
    private Label lblInput;                     // Label to display input prompt
    private HBox guessInput;                    // HBox pane to store input prompt and textfield
    private Gallows gallows;                    // Gallows pane for gallows and hangman
    private HBox gameBoard;                     // Game board pane for hangman phrase
    private VBox centerScreen;                  // Centerscreen pane to store gallows, game board, and guess input
    private Label lblOutput;                    // Label to display messages to user
    private HBox mistakesDisplay;               // HBox pane to display incorrect guesses and number of remaining attempts

    /**
     * Builds and initializes gameView pane
     */
    GameView() {
        guessedLetters = new ArrayList<>();
        wrongGuesses = new ArrayList<>();
        numMistakes = 0;

        // Displays incorrect guesses and guesses remaining
        mistakesDisplay = buildMistakesDisplay();
        updateMistakesDisplay(wrongGuesses);

        // Gallows and hangman
        gallows = new Gallows();

        // Mystery phrase display
        gameBoard = buildGameBoard();

        // Textfield for guess entry
        guessInput = buildGuessInput();

        // Add gallows, gameBoard, and guessInput in vertical order to pane
        centerScreen = new VBox();
        centerScreen.getChildren().addAll(gallows, gameBoard, guessInput);
        centerScreen.setSpacing(10);
        centerScreen.setAlignment(Pos.CENTER);
        centerScreen.setPadding(new Insets(5, 5, 5, 5));

        // To display messages to user
        lblOutput = new Label();
        lblOutput.setFont(Font.font("Georgia", 15));

        /*                          Top: Mistakes Display
                                 Center: Gallows, Game Board, Guess Input
                                    Bottom: Message output                          */
        setTop(mistakesDisplay);
        setCenter(centerScreen);
        setBottom(lblOutput);
        BorderPane.setAlignment(mistakesDisplay, Pos.CENTER);
        BorderPane.setAlignment(lblOutput, Pos.CENTER);
    }

    /**
     * Runs game logic on guess entry
     */
    private void play() {
        // Get character
        String guess = tfInput.getText();

        // If nothing was entered, return
        if (guess.length() == 0) {
            return;
        }

        // If more than one character was entered, take the first
        if (guess.length() > 1) {
            guess = guess.substring(0, 1);
        }

        // Reset input textfield
        tfInput.clear();

        // Input validation (letters only)
        String regex = "^[a-zA-Z]+$";
        if (!guess.matches(regex)) {
            lblOutput.setText("The aren't any of those in the phrase. Try a letter!");

            return;
        }

        // If the guess hasn't already been guessed, add to list of guessedLetters
        guess = guess.toLowerCase();
        if (guessedLetters.contains(guess)) {
            lblOutput.setText("You've already guessed this letter!");
            return;
        } else guessedLetters.add(guess);

        // If guess is in phrase, show correct guess on board
        boolean guessInPhrase = false;
        for (int i = 0; i < phrase.length(); i++) {
            if (guess.equalsIgnoreCase(phrase.substring(i, i + 1))) {
                VBox v = (VBox)gameBoard.getChildren().get(i);
                Text t = (Text)v.getChildren().get(0);
                t.setVisible(true);
                lblOutput.setText("Correct!");
                guessInPhrase = true;
            }
        }

        // Draw next body part if the guess was incorrect
        if (!guessInPhrase) {
            lblOutput.setText("Incorrect!");
            wrongGuesses.add(guess);
            numMistakes++;
            updateMistakesDisplay(wrongGuesses);
            gallows.drawNextPart(numMistakes);
        }

        // Win/lose conditions

        // Lose: check if hangman is hanged
        if (numMistakes == MAX_MISTAKES) {
            lblOutput.setText("How's it hangin' up there? (YOU LOSE)");

            LinkedList<Integer> missingLettersPos = new LinkedList<>();
            for (int i = 0; i < phrase.length(); i++) {
                VBox v = (VBox)gameBoard.getChildren().get(i);
                Text t = (Text)v.getChildren().get(0);

                if (!t.isVisible()) {
                    t.setVisible(true);
                    missingLettersPos.add(i);
                }
            }

            // Flash letters from AggieBlue to Red to indicate loss
            flashLetters(missingLettersPos, Color.rgb(6, 50, 67), Color.RED);

            // Open new game prompt
            offerNewGame();

            return;
        }

        // Win: check if all letters have been guessed
        boolean gameWon = true;
        for (int i = 0; i < phrase.length(); i++) {
            VBox v = (VBox)gameBoard.getChildren().get(i);
            Text t = (Text)v.getChildren().get(0);
            if (!t.getText().equals(" ") && !t.isVisible()) {
                gameWon = false;
                break;
            }
        }

        if (gameWon) {
            lblOutput.setText("Lucky guess. You win!");
            ArrayList<Integer> letterPos = new ArrayList<>(phrase.length());

            for (int i = 0; i < phrase.length(); i++) {
                letterPos.add(i);
            }

            // Flash letters from AggieBlue to green to indicate win
            flashLetters(letterPos, Color.rgb(6, 50, 67), Color.GREEN);

            // Open new game prompt
            offerNewGame();
        }
    }

    /**
     * Flash letters from c1 to c2, ending on c2
     */
    private void flashLetters(List<Integer> letterPos, Color c1, Color c2) {
        Timeline animation = new Timeline(new KeyFrame(Duration.millis(250), e -> {
            for (int p : letterPos) {
                VBox v = (VBox)gameBoard.getChildren().get(p);
                Text t = (Text)v.getChildren().get(0);

                if (t.getFill().equals(c1)) {
                    t.setFill(c2);
                } else t.setFill(c1);
            }
        }));
        animation.setCycleCount(11);
        animation.play();
    }

    /**
     * Prompt player to start new game
     */
    private void offerNewGame() {
        // Play again button
        Button play = new Button("PLAY AGAIN");
        play.setMaxHeight(Double.MAX_VALUE);

        // Exit game button
        Button exit = new Button("EXIT");
        exit.setMaxHeight(Double.MAX_VALUE);

        // Pane for buttons
        HBox options = new HBox(play, exit);
        options.setSpacing(5);
        options.setPadding(new Insets(5, 5, 5, 5));

        // Show menu to play again
        Stage playAgainStage = new Stage();
        playAgainStage.setScene(new Scene(options));
        playAgainStage.setResizable(false);
        playAgainStage.initStyle(StageStyle.UNDECORATED);
        playAgainStage.show();

        // Set play button to reset game
        play.setOnAction(e -> {
            playAgainStage.close();
            reset();
        });

        // Set exit button to exit the game
        exit.setOnAction(e -> System.exit(0));
    }

    /**
     * Reset GUI and game-state for new game
     */
    private void reset() {
        numMistakes = 0;
        guessedLetters.clear();
        wrongGuesses.clear();

        Stage primaryStage = (Stage) getScene().getWindow();
        Scene scene = getScene();
        GameView gameView = new GameView();
        scene.setRoot(gameView);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(scene.getHeight());
        primaryStage.setMinWidth(scene.getWidth());
        primaryStage.sizeToScene();
    }

    /**
     * Load random phrase from file
     */
    private String loadPhrase() {
        ArrayList<String> phrases = new ArrayList<>();
        try (Scanner input = new Scanner(new File("Phrases.txt"))) {
            while (input.hasNext()) {
                phrases.add(input.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int rand = (int)(Math.random() * phrases.size());
        return phrases.get(rand);
    }

    /**
     * Builds and returns pane for gameBoard (display for correctly guessed letters)
     * @return initialized gameBoard pane
     */
    private HBox buildGameBoard() {
        // Loads random phrase
        phrase = loadPhrase();

        // Text array for phrase letters
        Text[] text = new Text[phrase.length()];

        // Line array for blanks underneath letters
        Line[] blanks = new Line[phrase.length()];

        // VBox to store letter with blank underneath
        VBox[] cells = new VBox[phrase.length()];

        // HBox to store cells horizontally
        HBox gameBoard = new HBox();

        // Initializes text object for each letter
        for (int i = 0; i < phrase.length(); i++) {
            text[i] = new Text(phrase.substring(i, i + 1));
            text[i].setFont(Font.font("Verdana", FontWeight.BOLD, 20));
            text[i].setFill(Color.rgb(6, 50, 67));

            // Set invisible by default, to be set visible when correctly guessed
            text[i].setVisible(false);
            blanks[i] = new Line(0, 0, 20, 0);

            // Sets blanks under spaces invisible
            if (text[i].getText().equals(" ")) {
                blanks[i].setVisible(false);
            }

            // Add text and blanks to cell
            cells[i] = new VBox(text[i], blanks[i]);
            cells[i].setAlignment(Pos.CENTER);

            // Add cell to gameBoard
            gameBoard.getChildren().add(cells[i]);
        }

        gameBoard.setSpacing(5);
        gameBoard.setAlignment(Pos.CENTER);
        return gameBoard;
    }

    /**
     * Builds and returns guessInput HBox
     * @return initialized guessInput pane
     */
    private HBox buildGuessInput() {
        lblInput = new Label("Guess a letter: ");
        tfInput = new TextField();

        // Game logic will run when a letter is entered
        tfInput.setOnAction(e -> play());
        guessInput = new HBox();
        guessInput.getChildren().addAll(lblInput, tfInput);
        guessInput.setAlignment(Pos.CENTER);

        return guessInput;
    }

    /**
     * Builds and returns pane to display incorrect guesses and number of remaining attempts
     * @return initialized mistakesDisplay pane
     */
    private HBox buildMistakesDisplay() {
        Label lblWrongLetters = new Label();
        Label lblNumMistakes = new Label();

        HBox mistakesDisplay = new HBox();
        mistakesDisplay.getChildren().addAll(lblWrongLetters, lblNumMistakes);
        mistakesDisplay.setAlignment(Pos.CENTER);
        mistakesDisplay.setSpacing(20);

        return mistakesDisplay;
    }

    /**
     * Updates mistakes display on incorrect guess
     * @param wrongGuesses list of incorrect guesses made by user
     */
    private void updateMistakesDisplay(ArrayList<String> wrongGuesses) {
        StringBuilder s = new StringBuilder();
        s.append("Incorrect Guesses: ");
        for (String c : wrongGuesses) {
            s.append(c.toUpperCase());
            s.append(" ");
        }


        Label lblWrongLetters = (Label) mistakesDisplay.getChildren().get(0);
        lblWrongLetters.setText(s.toString());

        Label lblNumMistakes = (Label) mistakesDisplay.getChildren().get(1);
        lblNumMistakes.setText("Guesses Remaining: " + ((Integer) (MAX_MISTAKES - numMistakes)).toString());
    }
}
