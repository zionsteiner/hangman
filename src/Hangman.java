import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Hangman class to run hangman game in JavaFX GUI
 * @author ZionSteiner
 */
public class Hangman extends Application {
    private GameView gameView;  // gameView object constructs GUI and controls game-state/logic

    @Override
    public void start(Stage primaryStage) {
        // Pane construction and game control
        gameView = new GameView();

        Scene scene = new Scene(gameView);
        primaryStage.setTitle("Hangman");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setMinHeight(scene.getHeight());
        primaryStage.setMinWidth(scene.getWidth());
        primaryStage.sizeToScene();
    }
}
